import RPi.GPIO as GPIO
import time
import signal
import sys
from datetime import datetime

PIN_FISH_FEEDER = 18                                # board pin 18 which is GPIO 24 (where my fish feeder motor is connected)
SERVO_HERTZ = 50

GPIO.setmode(GPIO.BOARD)                            # reference board pin numbers, not GPIO numbers
GPIO.setup(PIN_FISH_FEEDER, GPIO.OUT)                

RIGHT_FAST = 5.0                                    # duty cycle speeds are found with testing out the individual motors :)
RIGHT_MID  = 5.9
RIGHT_SLOW = 6.2
STOP       = 6.8

servo_fish_feeder = None

def graceful_exit(sig, frame):
    print('Keyboard Control+C pressed!')
    if servo_fish_feeder != None:
        detact_servo()
        GPIO.cleanup()
    sys.exit(0)

signal.signal(signal.SIGINT, graceful_exit)                     # when Control+C is pressed, call graceful_exit

def attach_servo():
    global servo_fish_feeder
    servo_fish_feeder = GPIO.PWM(PIN_FISH_FEEDER, SERVO_HERTZ)
    servo_fish_feeder.start(0)                                      # start without any motion

def detact_servo():
    global servo_fish_feeder
    servo_fish_feeder.stop()

def feed_fish():
    global servo_fish_feeder
    attach_servo()
    servo_fish_feeder.ChangeDutyCycle(RIGHT_FAST)
    time.sleep(0.1)                                                 # spin the screw for 0.1 seconds
    servo_fish_feeder.ChangeDutyCycle(STOP)
    detact_servo()


if __name__ == '__main__':

    while True:
        feed_fish()
        time.sleep(1.0)

    GPIO.cleanup()