* Note: every time we update/upgrade something, close the terminal you're working in and 
        open a new terminal for the change to take effect!

1.  upgrade the operating system
    
    terminal > sudo apt update
             > sudo apt upgrade

2. enable robotic sensors and actuators (enable the libraries to read and write the
   Raspberry Pi's GPIO pins)

   Start > Preferences > Raspberry Pi Configuration > Interfaces

        (turn everything other than "Remote GPIO" to ON)

3. software install

    termainal > sudo apt install code
              > sudo apt install simplescreenrecorder

4. fix Python and the terminal

    terminal > cd ~
             > code .

    (edit the ".bashrc" file)

    * add the following lines to the bottom of the file:

        alias python=python3
        alias pip=pip3

    * setup the 'll' command

        uncomment the following line near the bottom of the file:

            # some more ls aliases
            alias ll='ls -l'
            #alias la='ls -A'
            #alias l='ls -CF'

5. Update Pip

    terminal > pip install --upgrade pip

    to test that this worked...

    terminal > python --version
    terminal > pip --version
